/*
 * The RIP License (Revision 0.3):
 * This software is available without warranty and without support.
 * Use at your own risk. Literally. It might delete your filesystem or
 * eat your cat. As long as you retain this notice, you can do whatever
 * you want with this. If we meet some day, you owe me a beer.
 *
 * Go Vols!
 *
 *  __    __  ________  __    __        _______   ______  _______
 * |  \  |  \|        \|  \  /  \      |       \ |      \|       \
 * | $$  | $$ \$$$$$$$$| $$ /  $$      | $$$$$$$\ \$$$$$$| $$$$$$$\
 * | $$  | $$   | $$   | $$/  $$       | $$__| $$  | $$  | $$__/ $$
 * | $$  | $$   | $$   | $$  $$        | $$    $$  | $$  | $$    $$
 * | $$  | $$   | $$   | $$$$$\        | $$$$$$$\  | $$  | $$$$$$$
 * | $$__/ $$   | $$   | $$ \$$\       | $$  | $$ _| $$_ | $$
 *  \$$    $$   | $$   | $$  \$$\      | $$  | $$|   $$ \| $$
 *   \$$$$$$     \$$    \$$   \$$       \$$   \$$ \$$$$$$ \$$
 */
#include "rip/datatypes/motor_dynamics.hpp"

#include <cmath>
namespace rip::datatypes
{
    MotorDynamics::MotorDynamics(double ticks_per_rev, inch_t wheel_radius)
        : m_distance(NAN)
        , m_speed(NAN)
        , m_acceleration(NAN)
        , m_deceleration(NAN)
    {
        m_conversion_factor = wheel_radius() / 3.14159265359 * 2 * ticks_per_rev;
    }

    void MotorDynamics::setDistance(inch_t distance)
    {
        if(distance() < 0.0)
        {
            throw OutOfRange("Distance should be a positive value.");
        } 
        m_distance = distance;
    }

    double MotorDynamics::getDistance() const
    {
        return m_distance() / m_conversion_factor;
    }

    void MotorDynamics::setSpeed(inches_per_second_t speed)
    {
        m_speed = speed;
    }

    double MotorDynamics::getSpeed() const
    {
        return m_speed() / m_conversion_factor;
    }

    void MotorDynamics::setAcceleration(inches_per_second_squared_t acceleration)
    {
        if(acceleration() < 0.0)
        {
            throw OutOfRange("Acceleration should be a positive value.");
        }
        m_acceleration = acceleration;
    }

    double MotorDynamics::getAcceleration() const
    {
        return m_acceleration() / m_conversion_factor;
    }

    void MotorDynamics::setDeceleration(inches_per_second_squared_t deceleration)
    {
        if(deceleration() < 0.0)
        {
            throw OutOfRange("Deceleration should be a positive value.");
        }
        m_deceleration = deceleration;
    }

    double MotorDynamics::getDeceleration() const
    {
        return m_deceleration() / m_conversion_factor;
    }

    MotorDynamics::DType MotorDynamics::getDType() const
    {

        DType d_type = static_cast< DType >(static_cast< uint8_t >(!(std::isnan(m_distance.to<double>()))) << 3 |
                                            static_cast< uint8_t >(!(std::isnan(m_distance.to<double>()))) << 2 |
                                            static_cast< uint8_t >(!(std::isnan(m_distance.to<double>()))) << 1 |
                                            static_cast< uint8_t >(!(std::isnan(m_distance.to<double>()))));
        switch(d_type)
        {
            case DType::kNone:
            case DType::kSpeed:
            case DType::kSpeedAccel:
            case DType::kSpeedAccelDist:
            case DType::kSpeedDist:
            case DType::kSpeedAccelDecelDist:
                break;
            default:
                throw UnknownDType();
        }

        return d_type;
    }
}  // namespace rip::datatypes
