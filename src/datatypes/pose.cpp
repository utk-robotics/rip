#include <rip/datatypes/pose.hpp>

namespace rip::datatypes
{
    using inch_t       = units::length::inch_t;
    using millimeter_t = units::length::millimeter_t;
    using degree_t     = units::angle::degree_t;

    Pose::Pose(const inch_t& x, const inch_t& y, const degree_t& theta)
        : m_x(x)
        , m_y(y)
        , m_theta(theta)
    {
    }

    inch_t Pose::x() const
    {
        return m_x;
    }

    inch_t Pose::y() const
    {
        return m_y;
    }

    degree_t Pose::theta() const
    {
        return m_theta;
    }
}  // namespace rip
