#include <rip/datatypes/laser_scan.hpp>

namespace rip::datatypes
{
    using inch_t   = units::length::inch_t;
    using degree_t = units::angle::degree_t;

    LaserScan::LaserScan(const inch_t& distance, const degree_t& angle)
        : m_distance(distance)
        , m_angle(angle)
    {
    }

    inch_t LaserScan::distance() const
    {
        return m_distance;
    }

    degree_t LaserScan::angle() const
    {
        return m_angle;
    }
}  // namespace rip
