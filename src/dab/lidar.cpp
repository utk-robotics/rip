#include "rip/dab/lidar.hpp"

namespace rip
{
    std::vector< LaserScan > Lidar::get()
    {
        std::lock_guard< std::mutex > lock(m_mutex);
        return m_last_scan;
    }
}
