#include <rip/event_system.hpp>
#include <rip/logger.hpp>

#include <algorithm>

namespace rip
{

    EventSystem::EventSystem(std::string stop_handle, unsigned int num_threads)
    : m_stop(stop_handle), m_running(false), m_num_threads(num_threads)
    , m_message_ready(false) 
    { ; }

    void EventSystem::start()
    {
        Logger::debug("EventSystem: Starting the EventLoop");
        // Creates an empty vector to use as data for the startup message
        std::vector<std::any> def;
        // Protects the message queue and adds the event-system:startup message.
        std::unique_lock<std::mutex> lk(m_message_mutex);
        m_messages.push(std::pair("startup:event-system", def));
        lk.unlock();
        // Flips the running state and creates the thread pool.
        m_running = true;
        m_threads = std::make_unique<threading::ThreadPool>((size_t)(m_num_threads));
        // Starts the event loop.
        runLoop();
    }

    void EventSystem::postMessage(std::string handle, std::string routine_id, 
            std::vector<std::any> data)
    {
        Logger::debug("EventSystem: Received message to {} from {}", handle, routine_id);
        // Computes the complete handle
        std::string complete_handle = handle + ":" + routine_id;
        // Protects the message queue and sends the message to both the
        // general handle and the Routine-specific handle.
        Logger::debug("EventSystem: Trying to lock m_message_mutex");
        std::unique_lock<std::mutex> lk(m_message_mutex);
        m_messages.push(std::pair(handle, data));
        m_messages.push(std::pair(complete_handle, data));
        m_message_ready = true;
        lk.unlock();
        // Notifies the event loop's condition variable to tell it to
        // reaquire the lock on m_message_mutex.
        Logger::debug("EventSystem: Unlocked m_message_mutex and notifying m_cv.");
        m_cv.notify_one();
    }

    std::string EventSystem::subscribe(std::string handle,
            std::function<void(std::vector<std::any>)> callback,
            std::string routine_issuer)
    {
        Logger::debug("EventSystem: Subscribing callback to {} from {}", handle, routine_issuer);
        // Determines the correct handle based on the value of routine_issuer
        std::string complete_handle = (routine_issuer=="") ? handle 
            : handle + ":" + routine_issuer;
        // Protects the callback hash table and adds the callback.
        std::unique_lock<std::mutex> lk(m_cback_mutex);
        auto iter = m_cbacks.find(complete_handle);
        if (iter == m_cbacks.end())
        {
            std::vector<std::function<void(std::vector<std::any>)> > vec;
            vec.push_back(callback);
            m_cbacks[complete_handle] = vec;
        }
        else
        {
            iter->second.push_back(callback);
        }
        // Returns the handle that the callback was actually registered under.
        return complete_handle;
    }

    void EventSystem::addRoutine(std::string handle, 
            std::function<std::shared_ptr<AbstractRoutine>()> creator,
            std::string routine_issuer)
    {
        Logger::debug("EventSystem: Subscribing routine constructor to {} from {}", 
                handle, routine_issuer);
        // Binds this and creator to EventSystem::createRoutine to create a callback.
        std::function<void(std::vector<std::any>)> cback = std::bind(&EventSystem::createRoutine,
                this, creator, std::placeholders::_1);
        // Subscribes this new callback to the Event System.
        subscribe(handle, cback, routine_issuer);
    }

    void EventSystem::removeRoutine(std::string key)
    {
        Logger::debug("EventSystem: Removing active Routine {}", key);
        // Searches for the key and removes it if it was found.
        auto iter = m_routines.find(key);
        if (iter != m_routines.end())
        {
            m_routines.erase(iter);
        }
        else
        {
            Logger::error("There was no Routine with key {} in the Event System.", key);
        }
    }

    void EventSystem::runLoop()
    {
        std::unique_lock<std::mutex> lk(m_message_mutex);
        // This loop continues until the Event System is shutdown.
        while (m_running)
        {
            Logger::debug("EventSystem: Messages = {}", m_messages.size());
            // Protects the message queue and checks if there are any messages.
            while (m_messages.empty())
            {
                Logger::debug("EventSystem: Waiting on Message.");
                // If there aren't any messages, unlocks m_message_mutex until
                // the condition variable is notified or m_running is set to false.
                m_cv.wait(lk, [this]{ return m_message_ready || !m_running; });
                Logger::debug("EventSystem: Messages = {}", m_messages.size());
            }
            m_message_ready = false;
            Logger::debug("EventSystem: Message has been detected in event loop.");
            // If the condition variable was deactivated by m_running being set to false,
            // unlocks the m_message_mutex (since it was reaquired after the condition
            // variable released its block) and shuts down the event system.
            if (!m_running)
            {
                lk.unlock();
                shutdown();
                break;
            }
            // Double checks that the message queue does have something in it.
            if (m_messages.empty())
            {
                continue;
            }
            // Processes all messages in the queue.
            while (!m_messages.empty())
            {
                // Grabs the oldest message and extracts the handle and data from it.
                auto packet = m_messages.front();
                std::string handle = std::get<0>(packet);
                std::vector<std::any> data = std::get<1>(packet);
                // Removes the current message from the queue.
                m_messages.pop();
                Logger::debug("EventSystem: Processing handle {}", handle);
                // Unlocks m_message_mutex so that messages can be added on while processing
                // the current message.
                //lk.unlock();
                // Protects the callback hash table and searches for the handle extracted
                // from the current message.
                std::unique_lock<std::mutex> lk2(m_cback_mutex);
                auto iter = m_cbacks.find(handle);
                // If the handle was not found, reaquires the lock on m_message_mutex
                // and continues to the next iteration of the loop.
                if (iter == m_cbacks.end())
                {
                    Logger::warn("EventSystem: Could not find {}", handle);
                    //lk.lock();
                    lk2.unlock();
                    if (handle == m_stop)
                    {
                        Logger::debug("EventSystem: Stop Handle detected (not in table)");
                        lk.unlock();
                        shutdown();
                        return;
                    }
                    continue;
                }
                // If the handle was found, launches all callbacks assigned to that handle,
                // each assigned to a different thread.
                else
                {
                    auto cbacks = iter->second;
                    for (auto call : cbacks)
                    {
                        m_threads->enqueue(call, data);
                    }
                }
                // If the handle was the shutdown handle, shuts down the Event System, and
                // exits the function to prevent further message handling.
                if (handle == m_stop)
                {
                    Logger::debug("EventSystem: Stop Handle detected (in table)");
                    lk2.unlock();
                    lk.unlock();
                    shutdown();
                    return;
                }
                // Unlocks m_cback_mutex and locks m_message_mutex to prevent data races and
                // wasted resources.
                lk2.unlock();
                //lk.lock();
            }
            Logger::debug("EventSystem: Finished processing current messages.");
        }
        Logger::debug("EventSystem: Event Loop has stopped.");
    }

    void EventSystem::shutdown()
    {
        Logger::debug("EventSystem: Shutting Down.");
        // Protects and empties the message queue.
        // Protects and empties the callback hash table.
        std::unique_lock<std::mutex> lk(m_cback_mutex);
        m_cbacks.clear();
        lk.unlock();
        Logger::debug("EventSystem: Callback table cleared");
        // Protects the Routine hash table, and invokes the stop function of all active Routines.
        // Then, empties the Routine hash table.
        lk = std::unique_lock<std::mutex>(m_rout_mutex);
        auto begin = m_routines.begin();
        auto end = m_routines.end();
        while (begin != end)
        {
            begin->second->stop();
            ++begin;
        }
        Logger::debug("EventSystem: Active Routines stopped.");
        m_routines.clear();
        lk.unlock();
        Logger::debug("EventSystem: Routine table cleared.");
        lk = std::unique_lock<std::mutex>(m_message_mutex);
        while (!m_messages.empty())
        {
            m_messages.pop();
        }
        lk.unlock();
        Logger::debug("EventSystem: Message queue cleared");
        // Triggers the destructor of the thread pool, causing all threads to join.
        Logger::debug("EventSystem: Joining threads.");
        m_threads = nullptr;
        // Sets m_running to false as a precaution.
        m_running = false;
    }

    void EventSystem::createRoutine(std::function<std::shared_ptr<AbstractRoutine>()> creator,
            std::vector<std::any> data)
    {
        Logger::debug("EventSystem: Creating new Routine");
        // Creates a new Routine using creator
        auto rout = creator();
        // Protects the Routine hash table and inserts the new routine into it.
        std::unique_lock<std::mutex> lk(m_rout_mutex);
        m_routines[rout->getId()] = rout;
        lk.unlock();
        Logger::debug("EventSystem: Routine {} starting", rout->getId());
        // Starts the Routine.
        rout->start(data);
    }

} // namespace rip
