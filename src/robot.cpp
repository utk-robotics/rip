#include "rip/robot.hpp"
#include "rip/host_buffer.hpp"

#include <cppfs/FileHandle.h>
#include <cppfs/fs.h>

std::function<bool(std::exception_ptr)> embMessengerCback = [&](std::exception_ptr eptr){
    try
    {
        std::rethrow_exception(eptr);
    }
    catch (const std::exception& e)
    {
        rip::Logger::error("{}", e.what());
        return false;
    }
};

namespace rip
{

    using emb::host::EmbMessenger;

    std::shared_ptr<nlohmann::json> configConstants = nullptr;
    std::shared_ptr<nlohmann::json> commandIds = nullptr;

    nlohmann::json loadJson(const std::string& config_filepath)
    {
        Logger::debug("Loading config at {}", config_filepath);
        cppfs::FileHandle config_file = cppfs::fs::open(config_filepath);
        if (!config_file.exists())
        {
            throw ConfigFileNotFound("Could not locate a config at {}.", config_filepath);
        }
        std::unique_ptr<std::istream> in = config_file.createInputStream();
        nlohmann::json config;
        (*in) >> config;
        return config;
    }

    Robot::Robot(const std::string& name, const std::string& config_filepath,
            std::string es_stop, unsigned int num_threads,
            std::shared_ptr<RoutineFactory> r_fact, std::shared_ptr<ComponentFactory> c_fact)
        : m_name(name)
        , m_routine_factory(r_fact)
        , m_component_factory(c_fact)
    {
        Logger::debug("Robot: Constructing Robot with config at {}", config_filepath);
        m_esystem = std::make_shared<EventSystem>(es_stop, num_threads);
        m_config = loadJson(config_filepath);
        if (m_config.find("constants") != m_config.end())
        {
            configConstants = std::make_shared<nlohmann::json>(m_config.at("constants"));
        }
        else
        {
            Logger::warn("Robot: Could not find \"constants\" section in config.");
        }
        if (m_config.find("commandIds") != m_config.end())
        {
            commandIds = std::make_shared<nlohmann::json>(m_config.at("commandIds"));
        }
        else
        {
            Logger::warn("Robot: Could not find \"comandIds\" section in config.");
        }
    }

    Robot::Robot(const std::string& name, nlohmann::json& config,
            std::string es_stop, unsigned int num_threads,
            std::shared_ptr<RoutineFactory> r_fact, std::shared_ptr<ComponentFactory> c_fact)
        : m_name(name)
        , m_routine_factory(r_fact)
        , m_component_factory(c_fact)
    {
        Logger::debug("Robot: Constructing Robot with provided config.");
        m_esystem = std::make_shared<EventSystem>(es_stop, num_threads);
        m_config = config;
        if (m_config.find("constants") != m_config.end())
        {
            configConstants = std::make_shared<nlohmann::json>(m_config.at("constants"));
        }
        else
        {
            Logger::warn("Robot: Could not find \"constants\" section in config.");
        }
        if (m_config.find("commandIds") != m_config.end())
        {
            commandIds = std::make_shared<nlohmann::json>(m_config.at("commandIds"));
        }
        else
        {
            Logger::warn("Robot: Could not find \"comandIds\" section in config.");
        }
    }

    std::string Robot::name() const
    {
        return m_name;
    }

    void Robot::loadMicrocontroller(std::string name, const nlohmann::json& config)
    {
        Logger::debug("Robot: Trying to load microcontroller {}", name);
        if (m_mcus.find(name) != m_mcus.end())
        {
            Logger::error("MCU {} already exists", name);
            return;
        }
        // TODO make this shared_ptr
        LibSerial::SerialPort* serial;
        const char* device;
        uint32_t baudrate;
        LibSerial::BaudRate lsBaud;

        baudrate = config.at("baudrate");

        std::string temp_device = config.at("device");

        switch (baudrate)
        {
            case 50: lsBaud = LibSerial::BaudRate::BAUD_50; break;
            case 75: lsBaud = LibSerial::BaudRate::BAUD_75; break;
            case 110: lsBaud = LibSerial::BaudRate::BAUD_110; break;
            case 134: lsBaud = LibSerial::BaudRate::BAUD_134; break;
            case 150: lsBaud = LibSerial::BaudRate::BAUD_150; break;
            case 200: lsBaud = LibSerial::BaudRate::BAUD_200; break;
            case 300: lsBaud = LibSerial::BaudRate::BAUD_300; break;
            case 600: lsBaud = LibSerial::BaudRate::BAUD_600; break;
            case 1200: lsBaud = LibSerial::BaudRate::BAUD_1200; break;
            case 1800: lsBaud = LibSerial::BaudRate::BAUD_1800; break;
            case 2400: lsBaud = LibSerial::BaudRate::BAUD_2400; break;
            case 4800: lsBaud = LibSerial::BaudRate::BAUD_4800; break;
            case 9600: lsBaud = LibSerial::BaudRate::BAUD_9600; break;
            case 19200: lsBaud = LibSerial::BaudRate::BAUD_19200; break;
            case 38400: lsBaud = LibSerial::BaudRate::BAUD_38400; break;
            case 57600: lsBaud = LibSerial::BaudRate::BAUD_57600; break;
            case 115200: lsBaud = LibSerial::BaudRate::BAUD_115200; break;
            case 230400: lsBaud = LibSerial::BaudRate::BAUD_230400; break;
#ifdef __linux__
            case 460800: lsBaud = LibSerial::BaudRate::BAUD_460800; break;
            case 500000: lsBaud = LibSerial::BaudRate::BAUD_500000; break;
            case 576000: lsBaud = LibSerial::BaudRate::BAUD_576000; break;
            case 921600: lsBaud = LibSerial::BaudRate::BAUD_921600; break;
            case 1000000: lsBaud = LibSerial::BaudRate::BAUD_1000000; break;
            case 1152000: lsBaud = LibSerial::BaudRate::BAUD_1152000; break;
            case 1500000: lsBaud = LibSerial::BaudRate::BAUD_1500000; break;
#if __MAX_BAUD > B2000000
            case 2000000: lsBaud = LibSerial::BaudRate::BAUD_2000000; break;
            case 2500000: lsBaud = LibSerial::BaudRate::BAUD_2500000; break;
            case 3000000: lsBaud = LibSerial::BaudRate::BAUD_3000000; break;
            case 3500000: lsBaud = LibSerial::BaudRate::BAUD_3500000; break;
            case 4000000: lsBaud = LibSerial::BaudRate::BAUD_4000000; break;
#endif
#endif
            default: throw InvalidBaudrate("A baudrate of {} is not valid", baudrate);
        }

        try
        {
            serial = new LibSerial::SerialPort(
                temp_device,
                lsBaud
            );
        }
        catch (const LibSerial::OpenFailed& of)
        {
            throw MicrocontrollerSerialOpenFail("Failed to open serial device {}", temp_device);
        }

        // TODO better check and new exceptions to match library
        if (!serial->IsOpen())
        {
            throw MicrocontrollerSerialOpenFail("Failed to open serial device {}", temp_device);
        }
        HostBuffer<64> buffer(serial);
        m_mcus[name] = std::make_shared<EmbMessenger>((emb::shared::IBuffer*)(&buffer), embMessengerCback);
    }

    void Robot::addRobotComponent(std::shared_ptr< RobotComponent > component, const std::string& name)
    {
        Logger::debug("Robot: Manually adding already created RobotComponent with name {}", name);
        // check if component already exists
        if(m_components.find(name) != m_components.end())
        {
            Logger::error("Component with name {} already exists", name);
        }
        m_components[name] = component;
    }

    void Robot::addMicrocontroller(std::shared_ptr<EmbMessenger> mcu,
            const std::string& name)
    {
        Logger::debug("Robot: Manually adding already created Microcontroller with name {}", name);
        // check if component already exists
        if(m_mcus.find(name) != m_mcus.end())
        {
            Logger::error("Microcontroller with name {} already exists", name);
        }
        m_mcus[name] = mcu;
    }

    template <typename T>
    void Robot::add(std::shared_ptr<T> to_add, const std::string& key)
    {
        if (std::is_same<T, EmbMessenger>::value)
        {
            addMicrocontroller(to_add, name);
        }
        else if (std::is_base_of<RobotComponent, T>::value)
        {
            addRobotComponent(to_add, key);
        }
        else
        {
            throw InvalidLookupType("Robot add function received invalid template parameter.");
        }
    }

    void Robot::loadComponent(std::string name, const nlohmann::json& config)
    {
        Logger::debug("Robot: Loading RobotComponent {} from JSON.", name);
        if (m_components.find(name) != m_components.end())
        {
            Logger::error("A RobotComponent with the name {} is already present.", name);
            return;
        }
        std::string key = config["factory_key"];
        std::shared_ptr<RobotComponent> comp;
        std::shared_ptr<std::unordered_map<std::string,
            std::shared_ptr<RobotComponent> > > comps_map;
        if (config.find("components") != config.end())
        {
            for (std::string id : config["components"])
            {
                if (m_components.find(id) == m_components.end())
                {
                    throw ComponentNotFound("Component {} not found.", name);
                }
                comps_map->emplace(id, m_components[id]);
            }
        }
        if (config.find("mcu") == config.end())
        {
            comp = m_component_factory->makeComponent(key, name, config, nullptr, comps_map);
        }
        else
        {
            comp = m_component_factory->makeComponent(key, name, config,
                    m_mcus[config["mcu"]], comps_map);
        }
        m_components[name] = comp;
    }

    void Robot::loadRoutine(std::string name, const nlohmann::json& config)
    {
        Logger::debug("Robot: Loading Routine {} from JSON.", name);
        if (m_routines.find(name) != m_routines.end())
        {
            Logger::error("A Routine with the name {} is already present.", name);
            return;
        }
        std::string key = config["factory_key"];
        std::function<std::shared_ptr<AbstractRoutine>()> func;
        std::shared_ptr<std::unordered_map<std::string,
            std::shared_ptr<RobotComponent> > > comps_map;
        if (config.find("components") != config.end())
        {
            for (std::string id : config["components"])
            {
                if (m_components.find(id) == m_components.end())
                {
                    throw ComponentNotFound("Component {} not found.", name);
                }
                comps_map->emplace(id, m_components[id]);
            }
        }
        func = std::bind(&RoutineFactory::makeRoutine,
            m_routine_factory, key, config, m_esystem, name, comps_map);
        m_esystem->addRoutine(config["handles"]["construct"][0], func,
                config["handles"]["construct"][1]);
        m_routines[name] = func;
    }

    void Robot::loadAllMicrocontrollers(const nlohmann::json& config)
    {
        Logger::debug("Robot: Loading all Microcontrollers from JSON.");
        for (auto [key, val] : config.items())
        {
            loadMicrocontroller(key, val);
        }
    }

    void Robot::loadAllComponents(const nlohmann::json& config)
    {
        Logger::debug("Robot: Loading all RobotComponents from JSON.");
        for (auto [key, val] : config.items())
        {
            loadComponent(key, val);
        }
    }

    void Robot::loadAllRoutines(const nlohmann::json& config)
    {
        Logger::debug("Robot: Loading all Routines from JSON.");
        for (auto [key, val] : config.items())
        {
            loadRoutine(key, val);
        }
    }

    void Robot::load(const nlohmann::json& config)
    {
        Logger::debug("Robot: Loading from JSON.");
        if (config.find("microcontrollers") != config.end())
        {
            loadAllMicrocontrollers(config["microcontrollers"]);
        }
        if (config.find("components") != config.end())
        {
            loadAllComponents(config["components"]);
        }
        if (config.find("routines") != config.end())
        {
            loadAllRoutines(config["routines"]);
        }
    }

    void Robot::load()
    {
        load(m_config);
    }

    void Robot::start()
    {
        if (m_esystem != nullptr)
        {
            m_esystem->start();
        }
    }

    void Robot::stop()
    {
        if (m_esystem != nullptr)
        {
            m_esystem->shutdown();
        }
    }

    template <typename T>
    std::shared_ptr<T> Robot::operator[](const std::string& key) const
    {
        if (std::is_base_of<EmbMessenger, T>::value)
        {
            return getMicrocontroller<T>(key);
        }
        else if (std::is_base_of<RobotComponent, T>::value)
        {
            return getRobotComponent<T>(key);
        }
        else if (std::is_base_of<Routine, T>::value)
        {
            return getRobotRoutine<T>(key);
        }
        else
        {
            throw InvalidLookupType("Robot [] operator received invalid template parameter.");
        }
    }

    const nlohmann::json Robot::getConfig() const
    {
        Logger::debug("Robot: returning the config.");
        return m_config;
    }

    template <typename T>
    std::shared_ptr<T> Robot::getMicrocontroller(const std::string& name) const
    {
        Logger::debug("Robot: returning the {} Microcontroller", name);
        if (m_mcus.find(name) == m_mcus.end())
        {
            throw UnknownMicrocontroller("Unknown microcontroller: {}", name);
            return nullptr;
        }
        return std::dynamic_pointer_cast<T>(m_mcus.at(name));
    }

    template <typename T>
    std::shared_ptr<T> Robot::getRobotComponent(const std::string& name) const
    {
        Logger::debug("Robot: returning the {} RobotComponent", name);
        if(m_components.find(name) == m_components.end())
        {
            throw UnknownComponent("Unknown component: {}", name);
        }
        return std::dynamic_pointer_cast<T>(m_components.at(name));
    }

    template <typename T>
    std::shared_ptr<T> Robot::getRobotRoutine(const std::string& name) const
    {
        Logger::debug("Robot: returning the {} Routine", name);
        if(m_routines.find(name) == m_routines.end())
        {
            throw UnknownRoutine("Unknown component: {}", name);
        }
        return std::dynamic_pointer_cast<T>(m_routines.at(name)());
    }

}  // namespace rip
