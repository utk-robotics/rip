#include "rip/component_factory.hpp"

namespace rip
{

    std::shared_ptr<RobotComponent> ComponentFactory::makeComponent(const std::string &key,
            const std::string& name, const nlohmann::json &config, 
            std::shared_ptr<emb::host::EmbMessenger> emb,
            CompTable comps)
    {
        // Invokes the Factory::create function to create the Routine.
        return Factory<RobotComponent, std::string, const std::string&, const nlohmann::json&, 
            std::shared_ptr<emb::host::EmbMessenger>,
            CompTable>::create(key, 
                    name, config, emb, comps);
    }

} // namespace rip
