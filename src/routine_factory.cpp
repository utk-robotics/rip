#include "rip/routine_factory.hpp"

namespace rip
{

    std::shared_ptr<AbstractRoutine> RoutineFactory::makeRoutine(const std::string &key, 
            const nlohmann::json &config, std::shared_ptr<EventSystem> es, std::string id,
            CompTable comps)
    {
        // Invokes the Factory::create function to create the Routine.
        return Factory<AbstractRoutine, std::string, const nlohmann::json&, 
            std::shared_ptr<EventSystem>, std::string, 
            CompTable>::create(key, 
                    config, es, id, comps);
    }

} // namespace rip
