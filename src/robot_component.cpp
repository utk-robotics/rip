#include "rip/robot_component.hpp"

namespace rip
{
    RobotComponent::RobotComponent(const std::string &name, const nlohmann::json &config,
            std::shared_ptr<emb::host::EmbMessenger> emb, 
            CompTable comps)
        : m_name(name)
        , m_messenger(emb)
    {
    }
    
    RobotComponent::RobotComponent(const std::string& name) : m_name(name)
    {
    }

    std::string RobotComponent::name() const
    {
        return m_name;
    }

    bool RobotComponent::runDiagnosticCommand(const std::string &command)
    {
        return m_diagnostic_functions[command]();
    }

    bool RobotComponent::runAllDiagnosticCommands()
    {
        for(const auto &function : m_diagnostic_functions)
        {
            if(function.second())
            {
                return true;
            }
        }
        return false;
    }
}  // namespace rip
