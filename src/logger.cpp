#include "rip/logger.hpp"

// Global
#include <vector>

// External
#include <cppfs/FileHandle.h>
#include <cppfs/fs.h>
#include <spdlog/sinks/ansicolor_sink.h>
#include <spdlog/spdlog.h>

namespace rip
{

    std::shared_ptr< spdlog::logger > Logger::getInstance()
    {
        // Only initialize once
        static bool first = true;
        if(first)
        {
            first = false;

            time_t now = time(0);

            // Create the logs folder if it does not exist
            cppfs::FileHandle logs_folder = cppfs::fs::open("logs");
            if(!logs_folder.exists())
            {
                logs_folder.createDirectory();
            }

            // Create logger sinks
            std::vector< spdlog::sink_ptr > sinks;
            sinks.push_back(std::make_shared< spdlog::sinks::ansicolor_stdout_sink_mt >());
            sinks.push_back(std::make_shared< spdlog::sinks::simple_file_sink_mt >(
                fmt::format("logs/{}.txt", asctime(localtime(&now)))));

            // Configure logger
            std::shared_ptr< spdlog::logger > logger =
                std::make_shared< spdlog::logger >(k_logger_name, begin(sinks), end(sinks));
            spdlog::set_pattern("[%H:%M:%S %z] [thread %t] [%I] %v");
            spdlog::register_logger(logger);
            spdlog::set_level(spdlog::level::debug);
        }

        return spdlog::get(k_logger_name);
    }
}  // namespace rip
