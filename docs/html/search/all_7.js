var searchData=
[
  ['laserscan',['LaserScan',['../dd/d28/classrip_1_1LaserScan.html',1,'rip']]],
  ['laserscan',['LaserScan',['../dd/d28/classrip_1_1LaserScan.html#a2f7388b003c0d455ea08c0d45743601b',1,'rip::LaserScan::LaserScan()=default'],['../dd/d28/classrip_1_1LaserScan.html#af1e52a79728d386f6339ad550e7e284a',1,'rip::LaserScan::LaserScan(const units::length::inch_t distance, const units::angle::degree_t angle)'],['../dd/d28/classrip_1_1LaserScan.html#aead313759c7e7aaefc1ef2424b1af901',1,'rip::LaserScan::LaserScan(const rplidar_response_measurement_node_t &amp;node)']]],
  ['lidar',['Lidar',['../d3/d45/classrip_1_1Lidar.html',1,'rip']]],
  ['logger',['Logger',['../d3/d9c/classrip_1_1Logger.html',1,'rip']]]
];
