var hierarchy =
[
    [ "rip::Callback< Args >::Bind", "db/da1/classrip_1_1Callback_1_1Bind.html", null ],
    [ "rip::Callback< Args >", "d2/df1/classrip_1_1Callback.html", null ],
    [ "rip::Callback< std::vector< Block > >", "d2/df1/classrip_1_1Callback.html", null ],
    [ "rip::Callback< std::vector< rip::LaserScan > >", "d2/df1/classrip_1_1Callback.html", null ],
    [ "rip::Event", "d1/dba/structrip_1_1Event.html", null ],
    [ "std::exception", null, [
      [ "rip::ExceptionBase", "d3/d60/classrip_1_1ExceptionBase.html", null ]
    ] ],
    [ "rip::LaserScan", "dd/d28/classrip_1_1LaserScan.html", null ],
    [ "rip::Lidar", "d3/d45/classrip_1_1Lidar.html", [
      [ "rip::RpLidar", "d0/d0f/classrip_1_1RpLidar.html", null ]
    ] ],
    [ "rip::Logger", "d3/d9c/classrip_1_1Logger.html", null ],
    [ "rip::Pixy", "dc/dfa/classrip_1_1Pixy.html", null ],
    [ "rip::Pose", "d5/d6f/classrip_1_1Pose.html", null ],
    [ "rip::RobotBase", "d9/df8/classrip_1_1RobotBase.html", null ],
    [ "rip::State", "d6/dfb/classrip_1_1State.html", null ],
    [ "rip::Subsystem", "d6/d79/classrip_1_1Subsystem.html", null ]
];