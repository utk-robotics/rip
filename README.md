![Power T](./docs/volts_shirt_2018.svg)

# Robotics Integrated Platform V2

The Robotics Integrated Platform is designed as an all encompassing library for small autonomous robots using a small microprocessor such as a raspberry pi as the primary controller with several other small modules running on microncontrollers.

## Dependencies
There are two ways to deal with the dependencies of RIP:

1. Use the provided Docker.
2. Install the packages natively on your local machine.

### Docker
See the [Docker Readme](docker/README.md)

### Native Install
RIP requires several dependencies:

1. CMake 3.1+
2. GCC/G++ 7.0+ or Clang 4+
3. LibSSH2 (`sudo apt install libssh2-1-dev`)
4. Zlib (`sudo apt install zlib1g-dev`)
5. LibUSB (`sudo apt install libusb-1.0.0-dev`)

RIP GUIs require:

1. QT5

## How to Contribute
All contributions should be made through our [Git workflow](https://github.com/utk-robotics-2017/rip/wiki/Git-Workflow) and following our [coding standards](https://github.com/utk-robotics-2017/rip/wiki/Coding-Standards).

Once your contributions are ready to be reviewed, please open a pull request to the dev branch. Pull Requests should have a small number of changes that can be easily reviewed by the experienced memebers. Ideally, mark your PR with the associated issues and the target milestone/version of RIP.

## Quick Start

### Get a terminal
On Linux, you have a terminal.
On Windows, you want to get something besides Cmd or PowerShell. Something like `Bash on Ubuntu on Windows`, `Mingw`, or `Msys2`. You'll also need some additional programs (e.g. cmake, g++, lcov, etc).

### This repo requires ssh
1. [Generate an ssh key](https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/)
2. [Add the ssh key to github](https://help.github.com/articles/adding-a-new-ssh-key-to-your-github-account/)
3. [Test your ssh connection to github](https://help.github.com/articles/testing-your-ssh-connection/)

### Clone the repo using ssh.
1. Click `Clone or download`
2. Clone with ssh. Make sure the box says `Clone with SSH` in the top left corner of the box, if it doesn't, click `Use SSH` in the top right corner to switch.
3. Click on the clipboard icon to copy the address to your clipboard.
4. In your favorite terminal, type in `git clone --recursive `, then paste the address from github, and hit enter.

### Switch to the branch you'll be working on.
If you'll be working on Arduino Gen, you would switch to one of the `arduino_gen` branches. For instance, if you'll be working on Arduino Gen Unit Tests, then you'll switch to `arduino_gen/unit_tests`.

### Setup CMake
1. Make a directory for the output files (i.e. `build` or `bin`), then go into it.
2. Build the project.
    * Linux: `cmake ..`
    * Windows: `cmake -G "Unix Makefiles" ..`

### Make
Type in `make` and you should start building rip, arduino_gen, etc.  
Make can be sped up by passing `-j<N>`, where is `N` is the number of cores on your computer.

### Build Script
Optionally, rather than setting up the CMake build yourself, you may opt to use the included `build-linux.sh` script which will automatically build RIP in `build/` with testing enabled.

## License
**The RIP License (Revision 0.4)**

This software is available without warranty and without support. Use at your own risk. Literally. It might delete your filesystem, eat your cat, or fail when the competition course is different from the practice course. As long as you retain this notice, you can do whatever you want with this. If we meet some day, you owe me a beer.

Go Vols!

![Flag](./docs/volts_flag_2018.svg)
