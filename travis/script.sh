#!/usr/bin/env bash
set -e

pushd build

# Checks if it compiles
make -j$(nproc )

if [[ $2 ]]; then
    BRANCH=$1
else
    BRANCH=$3
fi

### Done with tests
popd
