#ifndef LIDAR_HPP
#define LIDAR_HPP

// Global
#include <functional>
#include <vector>
#include <thread>
#include <memory>
#include <mutex>
#include <atomic>

// Local
#include <rip/datatypes/laser_scan.hpp>
#include <rip/robot_component.hpp>
using rip::datatypes::LaserScan;
namespace rip
{
    /**
     * @class Lidar
     * @brief Abstract base class for lidars
     */
    class Lidar : public RobotComponent
    {
    public:
        /**
         * @brief Pure virtual function that starts the lidar.
         *
         * @note This could entail starting the motor, connecting
         * the communication protocol, etc
         */
        virtual void start() = 0;

        /**
         * @brief Pure virtual function that stops the lidar.
         *
         * @note This could entail stopping the motor, disconnecting
         * the communication protocol, etc
         */
        virtual void stop() = 0;

        /**
         * @brief Pure virtual function that returns the most recent
         * set of scans from the lidar.
         */
        virtual std::vector< LaserScan > get();

    protected:
        /**
         * @brief The function for the background thread which receives
         * information from the lidar
         */
        virtual void run() = 0;

        std::atomic< bool > m_running;
        std::unique_ptr< std::thread > m_thread;
        std::mutex m_mutex;

        std::vector< LaserScan > m_last_scan;
    };

}  // namespace rip

#endif  // LIDAR_HPP
