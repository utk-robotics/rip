#ifndef DIFFERENTIAL_DRIVETRAIN_HPP
#define DIFFERENTIAL_DRIVETRAIN_HPP

// Local
#include <rip/robot_component.hpp>
#include <rip/datatypes/motor_dynamics.hpp>
#include "units.h"

using rip::datatypes::MotorDynamics;
using namespace units::length;
using namespace units::velocity;

namespace rip
{
    /**
     * Abstract base class for the drive train
     */
    class DifferentialDrivetrain : public RobotComponent 
    {
    public:
        DifferentialDrivetrain(const std::string& name, const nlohmann::json& config, 
            std::shared_ptr<emb::host::EmbMessenger> emb, 
            std::shared_ptr<std::unordered_map<std::string, 
            std::shared_ptr<RobotComponent> > > comps) 
            : RobotComponent(name, config, emb, comps) {} 

        DifferentialDrivetrain() {}
        /**
         * Drive all the motors
         * @param power [-1, 1]
         */
        virtual void drive(double power) = 0;

        /**
         * Drive left and right separately
         * @param left [-1, 1]
         * @param right [-1, 1]
         */
        virtual void drive(double left, double right) = 0;

        /**
         * Single command to all motors
         */
        virtual void drive(const MotorDynamics& command) = 0;

        /**
         * Command left and right sides separately
         */
        virtual void drive(const MotorDynamics& left, const MotorDynamics& right) = 0;

        /**
         * Reads encoders for every motor you tell it to read, reports back in respective
         * order
         */
        virtual std::vector<inch_t> readEncoders() = 0;
        /**
         * reads the encoder for one motor
         */
        virtual inch_t readEncoder(unsigned int motor) = 0;
        /**
         * Reads encoder velocity for every motor you tell it to read, reports back in respective
         * order
         * @param motors list of motors to read
         */
        virtual std::vector<inches_per_second_t> readEncoderVelocities() = 0;
        /**
         * reads the encoder for one motor
         */
        virtual inches_per_second_t readEncoderVelocity(unsigned int motor) = 0;
        /**
         * Resets a single encoder.
         */
        virtual void resetEncoder(unsigned int motor) = 0;
        /**
         * Resets all encoders.
         */
        virtual void resetAllEncoders();
    };  // namespace rip
}
#endif  // DRIVETRAIN_HPP
