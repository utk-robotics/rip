#ifndef HOST_BUFFER_HPP
#define HOST_BUFFER_HPP

#include <cstdio>

#include "rip/logger.hpp"

#include <EmbMessenger/IBuffer.hpp>
#include <EmbMessenger/DataType.hpp>
#include <libserial/SerialPort.h>

namespace rip
{
    NEW_RIP_EX(SerialOpenFailure);
    NEW_RIP_EX(InvalidHostBufferBaudrate);
}

template <uint8_t BufferSize>
class HostBuffer : public emb::shared::IBuffer
{
protected:
    LibSerial::SerialPort* m_serial;
    uint8_t m_buffer[BufferSize];
    uint8_t m_streamFront, m_readFront;
    uint8_t m_numberMessages;

public:
    HostBuffer(const char* device, uint32_t baudrate=9600)
    {
        LibSerial::BaudRate brate;
        switch (baudrate)
        {
            case 50: brate = LibSerial::BaudRate::BAUD_50; break;
            case 75: brate = LibSerial::BaudRate::BAUD_75; break;
            case 110: brate = LibSerial::BaudRate::BAUD_110; break;
            case 134: brate = LibSerial::BaudRate::BAUD_134; break;
            case 150: brate = LibSerial::BaudRate::BAUD_150; break;
            case 200: brate = LibSerial::BaudRate::BAUD_200; break;
            case 300: brate = LibSerial::BaudRate::BAUD_300; break;
            case 600: brate = LibSerial::BaudRate::BAUD_600; break;
            case 1200: brate = LibSerial::BaudRate::BAUD_1200; break;
            case 1800: brate = LibSerial::BaudRate::BAUD_1800; break;
            case 2400: brate = LibSerial::BaudRate::BAUD_2400; break;
            case 4800: brate = LibSerial::BaudRate::BAUD_4800; break;
            case 9600: brate = LibSerial::BaudRate::BAUD_9600; break;
            case 19200: brate = LibSerial::BaudRate::BAUD_19200; break;
            case 38400: brate = LibSerial::BaudRate::BAUD_38400; break;
            case 57600: brate = LibSerial::BaudRate::BAUD_57600; break;
            case 115200: brate = LibSerial::BaudRate::BAUD_115200; break;
            case 230400: brate = LibSerial::BaudRate::BAUD_230400; break;
#ifdef __linux__
            case 460800: brate = LibSerial::BaudRate::BAUD_460800; break;
            case 500000: brate = LibSerial::BaudRate::BAUD_500000; break;
            case 576000: brate = LibSerial::BaudRate::BAUD_576000; break;
            case 921600: brate = LibSerial::BaudRate::BAUD_921600; break;
            case 1000000: brate = LibSerial::BaudRate::BAUD_1000000; break;
            case 1152000: brate = LibSerial::BaudRate::BAUD_1152000; break;
            case 1500000: brate = LibSerial::BaudRate::BAUD_1500000; break;
#if __MAX_BAUD > B2000000
            case 2000000: brate = LibSerial::BaudRate::BAUD_2000000; break;
            case 2500000: brate = LibSerial::BaudRate::BAUD_2500000; break;
            case 3000000: brate = LibSerial::BaudRate::BAUD_3000000; break;
            case 3500000: brate = LibSerial::BaudRate::BAUD_3500000; break;
            case 4000000: brate = LibSerial::BaudRate::BAUD_2000000; break;
#endif // __MAX_BAUD
#endif // __linux__
            default: throw rip::InvalidHostBufferBaudrate("A baudrate of {} is not valid.", baudrate); break;
        }
        try
        {
            m_serial = new LibSerial::SerialPort(device, brate);
        }
        catch (const LibSerial::OpenFailed& of)
        {
            throw rip::SerialOpenFailure("Failed to open serial port {}", device);
        }
        if (!m_serial->IsOpen())
        {
            throw rip::SerialOpenFailure("Failed to open serial port {}", device);
        }
        m_streamFront = 0;
        m_readFront = 0;
        m_numberMessages = 0;
    }

    HostBuffer(const std::string& device, uint32_t baudrate)
        : HostBuffer(device.c_str(), baudrate)
    {}

    HostBuffer(LibSerial::SerialPort* serial)
    {
        m_serial = serial;
        m_streamFront = 0;
        m_readFront = 0;
        m_numberMessages = 0;
    }

    ~HostBuffer()
    {
        delete m_serial;
    }

    void writeByte(const uint8_t byte) override
    {
        // serial_write(m_serial, &byte, 1);
        rip::Logger::debug("Writting {0:x}", byte);
        m_serial->WriteByte(byte);
    }

    uint8_t peek() const override
    {
        return m_buffer[m_readFront];
    }

    uint8_t readByte() override
    {
        uint8_t byte = m_buffer[m_readFront];
        if (m_buffer[(BufferSize + m_readFront - 1) % BufferSize] == emb::shared::DataType::kEndOfMessage)
        {
            --m_numberMessages;
        }
        m_readFront = (m_readFront + 1) % BufferSize;
        return byte;
    }

    bool empty() const override
    {
        return m_streamFront == m_readFront;
    }

    size_t size() const override
    {
        return (BufferSize + m_streamFront - m_readFront) % BufferSize;
    }

    uint8_t messages() const override
    {
        return m_numberMessages;
    }

    void update() override
    {
        unsigned int input_count;
        while (m_serial->GetNumberOfBytesAvailable() > 0)
        {
            // HACK should find a way to read more than 1 byte at a time
            // while also checking for kEndOfMessage
            m_serial->ReadByte(m_buffer[m_streamFront], 250);
            rip::Logger::debug("Read {0:x}", m_buffer[m_streamFront]);
            if (m_buffer[(BufferSize + m_streamFront - 1) % BufferSize] == emb::shared::DataType::kEndOfMessage)
            {
                ++m_numberMessages;
            }
            m_streamFront = (m_streamFront + 1) % BufferSize;
        }
    }

    void zero() override
    {
        m_streamFront = 0;
        m_readFront = 0;
        m_numberMessages = 0;

        for (uint8_t i = 0; i < BufferSize; i++)
        {
            m_buffer[i] = 0;
        }
    }

    void print() const override
    {
        for (int i = 0; i < BufferSize; i += 16)
        {
            for (int j = 0; j < 16; j++)
            {
                printf("%02X ", m_buffer[i + j]);
            }
            printf("\n");
        }
        fflush(stdout);
    }
};

#endif // HOSTBUFFER_HPP
