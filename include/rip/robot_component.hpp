#ifndef ROBOT_COMPONENT_HPP
#define ROBOT_COMPONENT_HPP

// Global
#include <functional>
#include <memory>
#include <string>
#include <vector>
#include <unordered_map>

// External
#include <nlohmann/json.hpp>
#include <EmbMessenger/EmbMessenger.hpp>

// Local
#include <rip/exception_base.hpp>

NEW_RIP_EX(MaxEmbCommandsException)
static int m_command_id=0;

namespace rip
{

    NEW_RIP_EX(MissingRequiredComponent)

    /**
     * @brief Abstract base class for the RobotComponents
     *
     * A RobotComponent is a collection of sensors
     * and actuators that accomplish a task or
     * set of tasks together.
     *
     * @note Each RobotComponent has its own Finite State Machine
     */
    class RobotComponent
    {
    public:
        typedef std::shared_ptr<std::unordered_map<std::string,
                    std::shared_ptr<RobotComponent> > > CompTable;

        /**
         * @brief Constructor
         *
         * @param name The name of this RobotComponent
         */
        RobotComponent(const std::string& name, const nlohmann::json& config,
                std::shared_ptr<emb::host::EmbMessenger> emb,
                CompTable comps);

        RobotComponent(const std::string& name="component");


        /**
         * @brief Returns the name of RobotComponent
         *
         * @returns The name of the RobotComponent
         */
        std::string name() const;

        ///// Diagnostics /////
        /**
         * @brief Returns a list of the diagnostic commands
         *
         * @returns A list of the diagnostic commands
         */
        virtual std::vector< std::string > diagnosticCommands() const = 0;

        /**
         * @brief Runs an individual diagnostic command
         *
         * @param command The name of the diagnostic command to run
         */
        bool runDiagnosticCommand(const std::string& command);

        /**
         * @brief Runs all of the diagnostic commands for this RobotComponent
         */
        bool runAllDiagnosticCommands();

        /**
         * @brief Pure virtual stop function that should be overridden to stop a
         *        RobotComponent
         *
         * @note Individual appendages are stopped seperately
         */
        virtual void stop() = 0;

    protected:
        std::string m_name;
        std::unordered_map< std::string, std::function< bool(void) > > m_diagnostic_functions;
        std::shared_ptr<emb::host::EmbMessenger> m_messenger = nullptr;
    };

}  // namespace rip

typedef std::shared_ptr<std::unordered_map<std::string,
            std::shared_ptr<rip::RobotComponent> > > CompTable;

#endif  // RobotComponent_HPP
