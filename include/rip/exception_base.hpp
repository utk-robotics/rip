#ifndef EXCEPTION_BASE_HPP
#define EXCEPTION_BASE_HPP

// Global
#include <string>
#include <utility>

// External
#include <fmt/format.h>

/**
 * @brief Macro for creating a custom exception
 */
#define NEW_RIP_EX(name)                                                                  \
    class name : public rip::ExceptionBase                                            \
    {                                                                                 \
    public:                                                                           \
        template < typename... Args >                                                 \
        name(const std::string& message = "", Args&&... args)                         \
            : rip::ExceptionBase(#name ": " + message, std::forward< Args >(args)...) \
        {                                                                             \
        }                                                                             \
    };

namespace rip
{
    /**
     * @class ExceptionBase
     * @brief Base class for custom exceptions
     */
    class ExceptionBase : public std::exception
    {
    public:
        /**
         * @brief Constructor
         *
         * @note Forwards the message using fmt format
         */
        template < typename... Args >
        ExceptionBase(const std::string& message = "", Args&&... args)
            : m_message(fmt::format(message, std::forward< Args >(args)...))
        {
        }

        /**
         * @brief Returns the message from this exception
         *
         * @return The message from this exception
         */
        // const char* what() const throw();

    protected:
        std::string m_message;
    };

    // Common exceptions
    NEW_RIP_EX(ConfigException)
}  // namespace rip

#endif  // EXCEPTION_BASE_HPP
