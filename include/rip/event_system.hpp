#ifndef EVENT_SYSTEM_HPP
#define EVENT_SYSTEM_HPP

#include <condition_variable>
#include <functional>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>
#include <unordered_map>
#include <utility>

#include <rip/abstract_routine.hpp>
#include <ThreadPool.hpp>

namespace rip
{

    /**
     * @brief EventSystem controlls the execution of actions (Routines) for the robot. All actions are performed concurrently (using threading) within an event-driven control structure.
     *
     * <p> Handles:
     * @literal "startup:event-system": Starts the Event System and all Routines registered to activate on startup.
     */
    class EventSystem
    {
        public:

            /**
             * @brief Constructor
             *
             * @param stop_handle the handle on which to shutdown the Event System
             * @param num_threads the number of actions that the Robot can perform at a single time.
             */
            EventSystem(std::string stop_handle, unsigned int num_threads);

            /**
             * @brief Starts the Event System by pushing "startup:event-system" onto the message list.
             */
            void start();

            /**
             * @brief Posts a message to the general and Routine-specific handles, should either exist, containing the provided data.
             *
             * @param handle     the general handle to which the message should be sent.
             * @param routine_id the ID of the Routine sending the message.
             * @param data       the (optional) data sent in the message.
             */
            void postMessage(std::string handle, std::string routine_id, 
                    std::vector<std::any> data = {});

            /**
             * @brief Subscribes to a given handle using @p callback. If @p routine_issuer is not an empty string, the handle that @p callback is assigned to takes the form of "handle:issuer". Otherwise, @p callback is assigned to @p handle.
             *
             * @param handle         the general handle that the callback is assigned to.
             * @param callback       the callback function that is invoked when a message is posted to the corresponding handle.
             * @param routine_issuer the ID of the Routine that will emit the handle. Used to only listen to the provided handle if it comes from a certain Routine.
             *
             * @returns the full handle that @p callback is subscribed to. If @p routine_issuer is not an emtpy string, this value will be "handle:issuer". Otherwise, it will simply be @p handle.
             */
            std::string subscribe(std::string handle,
                    std::function<void(std::vector<std::any>)> callback, 
                    std::string routine_issuer = "");

            /**
             * @brief Adds a callback to the Event System that will construct a Routine.
             *
             * @param handle         the general handle that the callback is assigned to.
             * @param creator        the callback that creates a Routine. This should be an instance of the RoutineFactory::makeRoutine with all arguments bound.
             * @param routine_issuer the ID of the Routine that will emit the handle. Used to only listen to the provided handle if it comes from a certain Routine.
             */
            void addRoutine(std::string handle, 
                    std::function<std::shared_ptr<AbstractRoutine>()> creator,
                    std::string routine_issuer = "");

            /**
             * @brief Removes a Routine from internal storage.
             *
             * @param key the Routine's ID
             */
            void removeRoutine(std::string key);

            /// Shuts down the Event System
            void shutdown();

        private:

            // The function that runs the Event Systems event loop.
            void runLoop();

            // This function serves as a callback for Routine construction and startup.
            void createRoutine(std::function<std::shared_ptr<AbstractRoutine>()> creator, 
                    std::vector<std::any> data = {});

            // The handle that triggers EventSystem::shutdown
            const std::string m_stop;

            // The running state of the Event System
            std::atomic<bool> m_running;

            // The maximum number of threads
            int m_num_threads;

            // The mutex for protecting the callback hash table
            std::mutex m_cback_mutex;

            // The hash table that stores the callbacks
            std::unordered_map<std::string,
                std::vector<std::function<void(std::vector<std::any>)> > > m_cbacks;

            // The mutex for protecting the message queue
            std::mutex m_message_mutex;

            // A bool indicating if a message has been added.
            std::atomic<bool> m_message_ready;

            // The conditional variable for releasing the lock on the message mutex
            // in the EventSystem::runLoop function while the loop is waiting on a message.
            std::condition_variable m_cv;

            // The queue that stores messages
            std::queue<std::pair<std::string, std::vector<std::any> > > m_messages;

            // The mutex for protecting the Routine has table.
            std::mutex m_rout_mutex;

            // The hash table that stores the active Routines.
            std::unordered_map<std::string, std::shared_ptr<AbstractRoutine> > m_routines;

            // The thread pool for running callbacks.
            std::unique_ptr<threading::ThreadPool> m_threads;
    };

} // namespace rip

#endif // EVENT_SYSTEM_HPP
