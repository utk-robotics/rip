#ifndef ABSTRACT_ROUTINE_HPP
#define ABSTRACT_ROUTINE_HPP

#include <rip/exception_base.hpp>

#include <any>
#include <atomic>
#include <string>
#include <vector>

namespace rip
{

    /**
     * @brief AbstractRoutine is the top-level interface for Routines. It's purpose is to provide an interface for the Event System to interact with Routines without causing cyclical includes.
     */
    class AbstractRoutine
    {
        public:

            /**
             * @brief The top-level Routine constructor. It simply sets the running state and Routine ID.
             *
             * @param id the value used to set the Routine ID.
             */
            AbstractRoutine(std::string id) : m_running(false), m_id(id) { ; }

            virtual ~AbstractRoutine() { ; }

            /**
             * @brief Starts the action that the Routine performs.
             */
            virtual void start(std::vector<std::any> data = {}) = 0;

            /**
             * @brief Stops the Routine, usually by setting the running state to false.
             *
             * @param data a vector containing any data that might be needed.
             */
            virtual void stop(std::vector<std::any> data = {}) = 0;

            /**
             * @brief Checks if the Routine is actively running.
             *
             * @returns true if the Routine is active. False otherwise.
             */
            bool isRunning() { return m_running; }

            /**
             * @brief Gets a const copy of the Routine ID.
             *
             * @returns the Routine ID.
             */
            const std::string getId() { return m_id; }

        protected:

            // The running state of the Routine.
            std::atomic<bool> m_running;

            // The Routine ID.
            const std::string m_id;

    };

} // namespace rip

#endif // ABSTRACT_ROUTINE_HPP
