#ifndef ROUTINE_FACTORY_HPP
#define ROUTINE_FACTORY_HPP

#include <rip/exception_base.hpp>
#include <rip/routine.hpp>
#include <rip/utils/register.hpp>

#include <nlohmann/json.hpp>

#include <string>

namespace rip
{

    NEW_RIP_EX(NoSuchRoutine)

    /**
     * @brief The factory for creating Routines.
     */
    struct RoutineFactory
    {
        /// @brief Default constructor.
        RoutineFactory() = default;

        /**
         * @brief Registers a Routine of type @p Derived into the factory with a key of @p key.
         *
         * @param <Derived> the type of Routine being stored.
         * @param key       the string key used for identifying the Routine to be constructed.
         */
        template <typename Derived>
        void registerRoutine(const std::string &key)
        {
            // Registers Derived's constructor with Factory using key.
            Register_<AbstractRoutine, std::string, Derived, const nlohmann::json&,
            std::shared_ptr<EventSystem>, std::string, CompTable> 
                r(key);
            return;
        }

        /**
         * @brief Creates a new Routine usng the constructor stored at @p key.
         *
         * @param key    a string key used for internal lookup by the factory.
         * @param config the configuration JSON to be used in initializing the Routine.
         * @param es     a shared_ptr to the Event System.
         * @param comps  the components, if any, being passed to the Routine's constructor
         * @param id     the Routine's ID.
         *
         * @returns a shared_ptr to the created Routine, up-casted to an AbstractRoutine.
         */
        std::shared_ptr<AbstractRoutine> makeRoutine(const std::string &key, 
                const nlohmann::json &config, std::shared_ptr<EventSystem> es,
                std::string id, CompTable comps);
    };

}

#endif
