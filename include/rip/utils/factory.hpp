#ifndef FACTORY_HPP
#define FACTORY_HPP

// Global
#include <memory>
#include <unordered_map>
#include <utility>

namespace rip
{
    /**
     * @brief  Factory used to create instances of registered types derived from
     *         base
     *
     * @tparam Base The Base class type for this Factory
     * @tparam Key The type for the key used by this Factory to pull out a
     *         derived type
     * @tparam Args The arguments used in the constructor of the derivative
     */
    template < typename Base, typename Key, typename... Args >
    struct Factory
    {
        /**
         * @brief Create an instance based on the key
         */
        static std::unique_ptr< Base > create(const Key& key, Args... args)
        {
            auto iter = getMap().find(key);
            if(iter == getMap().end())
            {
                return nullptr;
            }

            // call factory function
            return iter->second(std::forward< Args >(args)...);
        }

        using MapType = std::unordered_map< Key, std::function< std::unique_ptr< Base >(Args...) > >;

        /**
         * @brief Singleton to guarantee initialization in other translation
         * units
         */
        static MapType& getMap()
        {
            static MapType map;
            return map;
        }
    };
}  // namespace rip

#endif  // FACTORY_HPP
