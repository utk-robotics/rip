#ifndef RIP_CASTING_HPP
#define RIP_CASTING_HPP

#include "rip/exception_base.hpp"

#include <any>
#include <cstdlib>
#include <cxxabi.h>
#include <typeinfo>
#include <type_traits>

namespace rip
{

    NEW_RIP_EX(BadAnyParamCast)

    /**
     * @brief Demangles the output of typeid(@p T).name() to make it human readable.
     *
     * @param <T> the type whose name is to be demangled.
     *
     * @returns the demangled name of type @p T, if it can be demangled, and the output of typeid(T).name otherwise.
     */
    template <typename T>
    std::string demangle_type()
    {
        typedef typename std::remove_reference<T>::type TR;
        int status = -4; // Assigned to eliminate a compiler warning
        std::unique_ptr<char, void(*)(void*)> demangler(abi::__cxa_demangle(typeid(TR).name(), nullptr, nullptr, &status), std::free);
        if (status != 0)
        {
            return typeid(T).name();
        }
        std::string r = demangler.get();
        if (std::is_lvalue_reference<T>::value)
        {
            r = "&" + r;
        }
        else if (std::is_rvalue_reference<T>::value)
        {
            r = "&&" + r;
        }
        if (std::is_volatile<TR>::value)
        {
            r = "volatile " + r;
        }
        if (std::is_const<TR>::value)
        {
            r = "const " + r;
        }
        return r;
    }

    /**
     * @brief Safely converts a value of std::any to type @p T. This is meant to be used to help in converting callback inputs from a std::vector<std::any> to the correct types.
     *
     * @param <T>   the type to cast to.
     * @param param the piece of data to be casted.
     *
     * @exception BadAnyParamCast if @p param cannot be safely cast to type @p T.
     * @returns @p param casted to type @p T.
     */
    template <typename T>
    T cast_param(std::any param)
    {
        try
        {
            T value = std::any_cast<T>(param);
            return value;
        }
        catch (const std::bad_any_cast &e)
        {
            throw BadAnyParamCast("Could not cast the paramater to type %s\n", demangle_type<T>().c_str());
        }
    }

}

#endif
