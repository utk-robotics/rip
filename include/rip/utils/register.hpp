#ifndef REGISTER_HPP
#define REGISTER_HPP

// Global
#include <memory>
#include <utility>

// Local
#include "rip/utils/factory.hpp"

namespace rip
{
    /**
     * @brief Registers a factory function for creating a type derived from base
     *
     * @tparam Base The base class type for this type of Factory
     * @tparam Key The type for the key used to determine which derivative
     * @tparam Derived The derived class type
     * @tparam Args The types of the arguments for the constructor of the
     *         Derived type
     */
    template < typename Base, typename Key, typename Derived, typename... Args >
    struct Register_
    {
        /**
         * @brief Constructor
         *
         * @param name The key for registering to the factory
         */
        Register_(const Key& name)
        {
            Factory< Base, Key, Args... >::getMap()[name] = &createBase;
        }

        /**
         * @brief Used for a pointer to factory function
         *
         * @param args The arguments
         */
        static auto createBase(Args... args)
        {
            return std::make_unique< Derived >(std::forward< Args >(args)...);
        }
    };
}  // namespace rip

#endif  // REGISTER_HPP
