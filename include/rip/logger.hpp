#ifndef LOGGER_HPP
#define LOGGER_HPP

// Global
#include <memory>
#include <utility>
#include <spdlog/spdlog.h>

namespace spdlog
{
    class logger;
}

namespace rip
{
    /**
     * @class Logger
     * @brief Wrapper for spdlog that sets up a logger that writes to both the
     * console and a file.
     *
     * @note This can be accessed anywhere in the program.
     */
    class Logger
    {
    private:
        /**
         * @brief Returns the singleton logger
         *
         * @returns The singleton logger
         */
        static std::shared_ptr< spdlog::logger > getInstance();
        static constexpr const char* k_logger_name = "rip_logger";

    public:
        /**
         * @brief Writes a message to the logger using fmt formating with
         * the debug level
         *
         * @param message The format string for the log message
         * @param args The arguments to be format into the log message
         *
         * @tparam Args A list of argument types to be formatted into the log
         * message
         */
        template < typename... Args >
        static void debug(const std::string& message, Args&&... args)
        {
            getInstance()->debug(message.c_str(), std::forward< Args >(args)...);
        }

        /**
         * @brief Writes a message to the logger using fmt formatting with
         * the info level
         *
         * @param message The format string for the log message
         * @param args The arguments to be format into the log message
         *
         * @tparam Args A list of argument types to be formatted into the log
         * message
         */
        template < typename... Args >
        static void info(const std::string message, Args&&... args)
        {
            getInstance()->info(message.c_str(), std::forward< Args >(args)...);
        }

        /**
         * @brief Writes a message to the logger using fmt formatting with
         * the warn level
         *
         * @param message The format string for the log message
         * @param args The arguments to be format into the log message
         *
         * @tparam Args A list of argument types to be formatted into the log
         * message
         */
        template < typename... Args >
        static void warn(const std::string message, Args&&... args)
        {
            getInstance()->warn(message.c_str(), std::forward< Args >(args)...);
        }

        /**
         * @brief Writes a message to the logger using fmt formatting with
         * the error level
         *
         * @param message The format string for the log message
         * @param args The arguments to be format into the log message
         *
         * @tparam Args A list of argument types to be formatted into the log
         * message
         */
        template < typename... Args >
        static void error(const std::string message, Args&&... args)
        {
            getInstance()->error(message.c_str(), std::forward< Args >(args)...);
        }

        /**
         * @brief Writes a message to the logger using fmt formatting with
         * the critical level
         *
         * @param message The format string for the log message
         * @param args The arguments to be format into the log message
         *
         * @tparam Args A list of argument types to be formatted into the log
         * message
         */
        template < typename... Args >
        static void critical(const std::string message, Args&&... args)
        {
            getInstance()->critical(message.c_str(), std::forward< Args >(args)...);
        }

    };
}  // namespace rip
#endif  // LOGGER_HPP
