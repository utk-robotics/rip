#ifndef ROBOT_HPP
#define ROBOT_HPP

// External
#include <nlohmann/json.hpp>

// Local
#include "rip/datatypes/constants.hpp"
#include "rip/robot_component.hpp"
#include "rip/logger.hpp"
#include "rip/component_factory.hpp"
#include "rip/routine_factory.hpp"

namespace rip
{

    NEW_RIP_EX(ConfigFileNotFound)

    NEW_RIP_EX(UnknownMicrocontroller)

    NEW_RIP_EX(UnknownComponent)

    NEW_RIP_EX(ComponentNotFound)

    NEW_RIP_EX(UnknownRoutine)

    NEW_RIP_EX(InvalidLookupType)

    NEW_RIP_EX(MicrocontrollerSerialOpenFail)

    NEW_RIP_EX(InvalidBaudrate)

    nlohmann::json loadJson(const std::string& config_filepath);

    /**
     * @class Robot
     * @brief A partial abstract base class for a robot
     */
    class Robot
    {
    public:
        Robot(const std::string& name="robot")
            : m_name(name)
            , m_esystem(nullptr)
        {
        }

        /**
         * @brief Constructor
         *
         * @param name The name of the robot
         * @param config_filepath The filepath to the configuraton json file
         */
        Robot(const std::string& name, const std::string& config_filepath, 
                std::string es_stop, unsigned int num_threads,
                std::shared_ptr<RoutineFactory> r_fact=nullptr, 
                std::shared_ptr<ComponentFactory> c_fact=nullptr);

        Robot(const std::string& name, nlohmann::json& config, 
                std::string es_stop, unsigned int num_threads,
                std::shared_ptr<RoutineFactory> r_fact=nullptr, 
                std::shared_ptr<ComponentFactory> c_fact=nullptr);

        std::string name() const;

        virtual void loadMicrocontroller(std::string name, const nlohmann::json& config);

        virtual void loadComponent(std::string name, const nlohmann::json& config);

        virtual void loadRoutine(std::string name, const nlohmann::json& config);

        virtual void loadAllMicrocontrollers(const nlohmann::json& config);

        virtual void loadAllComponents(const nlohmann::json& config);

        virtual void loadAllRoutines(const nlohmann::json& config);

        virtual void load(const nlohmann::json& config);

        virtual void load();

        /**
         * Starts the robot by sending the Event Subsystem's start Event
         */
        virtual void start();

        virtual void stop();

        template <typename T>
        std::shared_ptr<T> operator[](const std::string& key) const;

        template <typename T>
        void add(std::shared_ptr<T> to_add, const std::string& key);

        const nlohmann::json getConfig() const;

    protected:
        template <typename T>
        std::shared_ptr<T> getMicrocontroller(const std::string& name) const;
        
        template <typename T>
        std::shared_ptr<T> getRobotComponent(const std::string& name) const;

        template <typename T>
        std::shared_ptr<T> getRobotRoutine(const std::string& name) const;

        void addRobotComponent(std::shared_ptr< RobotComponent > component, const std::string& name);

        void addMicrocontroller(std::shared_ptr<emb::host::EmbMessenger> mcu, const std::string& name);

        std::unordered_map<std::string, std::shared_ptr<RobotComponent> > m_components;

        std::unordered_map<std::string, std::function<std::shared_ptr<AbstractRoutine>()> > m_routines;

        std::unordered_map<std::string, std::shared_ptr<emb::host::EmbMessenger> > m_mcus;

        std::string m_name;

        std::shared_ptr<RoutineFactory> m_routine_factory;
        
        std::shared_ptr<ComponentFactory> m_component_factory;

        std::shared_ptr<EventSystem> m_esystem;

        nlohmann::json m_config;
    };
}  // namespace rip

#endif  // ROBOT_HPP
