#ifndef CALLBACK_HPP
#define CALLBACK_HPP

// Global
#include <functional>
#include <list>
#include <memory>
#include <set>
#include <utility>


namespace rip
{
    /**
     * @class Callback
     * @brief Callbacks allow for multiple function to be executed
     *        response to an Callback being triggered.
     *
     * @tparam Args A list of arguments that the callback function will have
     */
    template < typename... Args >
    class Callback
    {
    public:
        using Function = std::function< void(Args...) >;

    private:
        using FunctionList     = std::list< std::shared_ptr< Function > >;
        using WeakFunctionList = std::list< std::weak_ptr< Function > >;

    public:
        /**
         * @class Bind
         * @brief Handles temporary bindings of functions to callback objects
         */
        class Bind
        {
        public:
            /**
             * @brief Destructor
             */
            ~Bind()
            {
                if(m_valid)
                {
                    m_callback.m_binds.erase(this);
                    m_callback.m_bound_functions.erase(m_bound_function_iterator);
                }
            }

        private:
            friend class Callback< Args... >;

            /**
             * @brief Contructor
             */
            Bind(Callback& callback, typename FunctionList::iterator bound_function_iterator)
                : m_callback(callback)
                , m_bound_function_iterator(bound_function_iterator)
                , m_valid(true)
            {
            }

            /**
             * @brief Invalidates this binding
             */
            void invalidate()
            {
                m_valid = false;
                // Should this be cleaned up here?
            }

            Callback& m_callback;
            typename FunctionList::iterator m_bound_function_iterator;
            bool m_valid;
        };

        /**
         * Default Constructor
         */
        Callback() = default;


        /**
         * @brief Destructor
         */
        ~Callback()
        {
            // Any remaining binds should be invalidated
            for(auto bind : m_binds)
            {
                bind->invalidate();
            }
        }

        /**
         * @brief Permanently binds a function to the Callback.
         *
         * @param function The function to bind
         *
         * @note Useful for instances in which the bound function will
         * never become invalid within the lifetime of the
         * Callback.
         */
        void permanentBind(const Function& function)
        {
            m_bound_functions.emplace_back(std::make_shared< Function >(function));
        }

        /**
         * Binds a function to the Callback temporarily
         *
         * @param function The function to bind
         */
        std::shared_ptr< Bind > bind(const Function& function)
        {
            m_bound_functions.emplace_back(std::make_shared< Function >(function));
            auto bound_function_iterator = --m_bound_functions.end();
            std::shared_ptr< Bind > bind(new Bind(*this, bound_function_iterator));
            m_binds.insert(bind.get());
            return bind;
        }

        /**
         * @brief Triggers the functions bound to this callback
         *
         * @param args The arguments to pass to the functions
         */
        void trigger(Args... args)
        {
            WeakFunctionList weak_functions;
            for(auto& ptr : m_bound_functions)
            {
                weak_functions.emplace_back(ptr);
            }
            for(auto& weak_ptr : weak_functions)
            {
                if(auto shared_ptr = weak_ptr.lock())
                {
                    (*shared_ptr)(std::forward< Args >(args)...);
                }
            }
        }

    private:
        friend class Bind;

        FunctionList m_bound_functions;
        std::set< Bind* > m_binds;
    };
}  // namespace rip

#endif  // EVENT_HPP
