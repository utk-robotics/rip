#ifndef ROUTINE_HPP
#define ROUTINE_HPP

#include <rip/event_system.hpp>
#include <rip/robot_component.hpp>
#include <rip/utils/casting.hpp>

// STL Includes
#include <cassert>

#include <nlohmann/json.hpp>

namespace rip
{

    NEW_RIP_EX(InvalidCallbackDataSize)

    NEW_RIP_EX(InvalidFunctionId)

    NEW_RIP_EX(InvalidConfig)

    /**
     * @brief Routine is the direct interface for all Routines. This interface inherits from the AbstractRoutine interface.
     */
    class Routine : public AbstractRoutine
    {
        public:

            /// @brief Default Destructor
            virtual ~Routine() { ; }

            /**
             * @brief Makes a subscription in the Event System using the general handle and ID for the callback.
             *
             * @param handle  the general handle to subscribe to.
             * @param func_id an ID for the function to be used as a callback. All possible values for this parameter should be hardcoded in the specific Routine.
             */
            void subscribe(std::string handle, std::string func_id)
            {
                handle_subscription(handle, func_id);
            }

            /**
             * @brief Makes a subscription in the Event System using the general handle, the ID of the Routine emitting the handle, and ID for the callback.
             *
             * @param handle     the general handle to subscribe to.
             * @param routine_id the ID for the Routine that emits @p handle.
             * @param func_id    an ID for the function to be used as a callback. All possible values for this parameter should be hardcoded in the specific Routine.
             */
            void subscribe(std::string handle, std::string routine_id, std::string func_id)
            {
                handle_subscription(handle, func_id, routine_id);
            }

            /**
             * @brief Sends a message to @p handle and "handle:m_id".
             *
             * @param handle the general handle to send the message to.
             * @param data   the data to be sent with the message.
             */
            void sendMessage(std::string handle, std::vector<std::any> data = {})
            {
                m_esystem->postMessage(handle, m_id, data);
            }

        protected:

            /**
             * @brief Routine Constructor.
             *
             * @param config the config to be used to setup the Routine.
             * @param es     a std::shared_ptr to the Event System.
             * @param id     the Routine's ID.
             */
            Routine(const nlohmann::json &config, std::shared_ptr<EventSystem> es, std::string id,
                    CompTable comps)
            : AbstractRoutine(id), m_esystem(es), m_config(config)
            {
                if (config.find("handles") != config.end())
                {
                    for (auto& [func, data] : config["handles"].items())
                    {
                        if (func != std::string("construct"))
                        {
                            subscribe(data[0], data[1], func);
                        }
                    }
                }
            }

            virtual void saveComponents(
                    CompTable comps) = 0;

            /**
             * @brief Prepares a callback function based on @p func_id, and makes a subscription in the event system using @p handle, @p routine_id, and the callback.
             */
            virtual void handle_subscription(std::string handle, std::string func_id, std::string routine_id="") = 0;

            /**
             * @brief Runs the action that the Routine represents.
             */
            virtual void run() = 0;

            // A list of the Routine's subscriptions.
            std::vector<std::string> m_subscriptions;

            // A shared_ptr to the Event System.
            std::shared_ptr<EventSystem> m_esystem;
            // The config used for initializing the Routine.
            const nlohmann::json m_config;
    };

} // namespace rip

#endif // ROUTINE_HPP
