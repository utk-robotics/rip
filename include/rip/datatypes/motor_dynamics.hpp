
/*
 * The RIP License (Revision 0.3):
 * This software is available without warranty and without support.
 * Use at your own risk. Literally. It might delete your filesystem or
 * eat your cat. As long as you retain this notice, you can do whatever
 * you want with this. If we meet some day, you owe me a beer.
 *
 * Go Vols!
 *
 *  __    __  ________  __    __        _______   ______  _______
 * |  \  |  \|        \|  \  /  \      |       \ |      \|       \
 * | $$  | $$ \$$$$$$$$| $$ /  $$      | $$$$$$$\ \$$$$$$| $$$$$$$\
 * | $$  | $$   | $$   | $$/  $$       | $$__| $$  | $$  | $$__/ $$
 * | $$  | $$   | $$   | $$  $$        | $$    $$  | $$  | $$    $$
 * | $$  | $$   | $$   | $$$$$\        | $$$$$$$\  | $$  | $$$$$$$
 * | $$__/ $$   | $$   | $$ \$$\       | $$  | $$ _| $$_ | $$
 *  \$$    $$   | $$   | $$  \$$\      | $$  | $$|   $$ \| $$
 *   \$$$$$$     \$$    \$$   \$$       \$$   \$$ \$$$$$$ \$$
 */
#ifndef MOTOR_DYNAMICS_HPP
#define MOTOR_DYNAMICS_HPP

#include <memory>
#include "rip/exception_base.hpp"
#include "units.h"
namespace units
{
    UNIT_ADD(velocity, inches_per_second, inches_per_second, ips, compound_unit<length::inches, inverse<time::seconds>>)
    UNIT_ADD(acceleration, inches_per_second_squared, inches_per_second_squared, ips_sq, compound_unit<length::inches, inverse<squared<time::seconds>>>)
}
using namespace units::length;
using namespace units::velocity;
using namespace units::acceleration;

namespace rip::datatypes
{
    NEW_RIP_EX(OutOfRange)
    NEW_RIP_EX(UnknownDType)
    /**
     * @class MotorDynamics
     * @brief Used as a container and safety check when performing encoder based
     * driving of motors. Takes units in inches for distance, 
     * inches / s for velocity, and inches / s^2 for acceleration / deceleration. 
     * Returns double values that represent distance as ticks.
     */
    class MotorDynamics
    {
    public:
        
        MotorDynamics(double ticks_per_rev, inch_t wheel_radius);

        /**
         * @brief Sets the distance for the motor dynamics 
         * Must be a positive value.
         * @param distance linear distance to move in inches
         */
        void setDistance(inch_t distance);

        /**
         * @brief Returns distance in ticks.
         * @returns NaN if not set, or ticks of distance
         */
        double getDistance() const;

        /**
         * @brief Sets the speed. This is a signed value unlike the 
         * others.
         * @param speed
         */
        void setSpeed(inches_per_second_t speed);

        /**
         * @brief Returns the speed
         * @returns NaN if not set or the speed in ticks.
         */
        double getSpeed() const;

        /**
         * @brief Sets the acceleration for the motor dynamics 
         * Must be a positive value.
         * @param acceleration acceleration in inches / second^2
         */
        void setAcceleration(inches_per_second_squared_t acceleration);
        /**
         * @brief Returns acceleration in ticks.
         * @returns NaN if not set, or ticks / second ^2.
         */
        double getAcceleration() const;

        /**
         * @brief Sets the deceleration for the motor dynamics 
         * Must be a positive value.
         * @param acceleration acceleration in inches / second^2
         */
        void setDeceleration(inches_per_second_squared_t deceleration);

        /**
         * @brief Returns deceleration in ticks.
         * @returns NaN if not set, or ticks / second ^2.
         */
        double getDeceleration() const;

        enum class DType
        {
            kNone                = 0x0,
            kSpeed               = 0x8,
            kSpeedAccel          = 0xC,
            kSpeedDist           = 0x9,
            kSpeedAccelDist      = 0xD,
            kSpeedAccelDecelDist = 0xF
        };

        DType getDType() const;

    private:
        inch_t m_distance;
        inches_per_second_t m_speed;
        inches_per_second_squared_t m_acceleration;
        inches_per_second_squared_t m_deceleration;
        double m_conversion_factor;
    }; // class MotorDynamics
}
#endif // MOTOR_DYNAMICS_HPP
