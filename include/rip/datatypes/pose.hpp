#ifndef POSE_HPP
#define POSE_HPP

// External
#include <units.h>

namespace rip::datatypes
{
    /**
     * @class Pose
     * @brief The pose (position and orientation) of a robot in 2D space
     */
    class Pose
    {
        using inch_t   = units::length::inch_t;
        using degree_t = units::angle::degree_t;

    public:
        /**
         * @brief Default Constructor
         */
        Pose() = default;

        /**
         * Constructor
         *
         * @param x The x component of the position
         * @param y The y component of the position
         * @param theta The orientation
         */
        Pose(const inch_t& x, const inch_t& y, const degree_t& theta);

        /**
         * @brief Conversion Constructor for BreezySlam
         *
         * @param position The position from BreezySlam
         */
        // Pose(const Position& position);

        /**
         * @brief Returns the x component of the position
         *
         * @returns The x component of the position
         */
        inch_t x() const;

        /**
         * @brief Returns the y component of the position
         *
         * @returns The y component of the position
         */
        inch_t y() const;

        /**
         * @brief Returns the orientation
         *
         * @returns The orientation
         */
        degree_t theta() const;

    protected:
        inch_t m_x;
        inch_t m_y;
        degree_t m_theta;
    };
}  // namespace rip

#endif  // POSE_HPP
