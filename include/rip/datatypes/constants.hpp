#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

#include <nlohmann/json.hpp>

namespace rip
{
    extern std::shared_ptr<nlohmann::json> configConstants;

    extern std::shared_ptr<nlohmann::json> commandIds;
}

#endif
