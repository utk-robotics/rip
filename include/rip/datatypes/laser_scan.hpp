#ifndef SCAN_HPP
#define SCAN_HPP

// External
#include <units.h>

namespace rip::datatypes
{
    /**
     * @class LaserScan
     * @brief Data class for a single laser measurement
     */
    class LaserScan
    {
        using inch_t   = units::length::inch_t;
        using degree_t = units::angle::degree_t;
    public:
        /**
         * Constructor for vectors
         *
         * @note Should not be used directly
         */
        LaserScan() = default;

        /**
         * @brief Constructor
         *
         * @param distance The distance the laser went before contacting
         * something
         * @param angle The angle the lidar is facing
         */
        LaserScan(const inch_t& distance, const degree_t& angle);

        /**
         * @brief Returns the distance
         *
         * @returns The distance from this scan
         */
        inch_t distance() const;

        /**
         * @brief Returns the angle
         *
         * @returns The angle the lidar was facing when it took this scan
         */
        degree_t angle() const;

    protected:
        inch_t m_distance;
        degree_t m_angle;
    };
}  // namespace rip

#endif  // SCAN_HPP
